package cl.clases.demo2.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import cl.clases.demo2.model.Usuario;
import cl.clases.demo2.repo.UsuarioRepo;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioRepo repo;
	
	@PostMapping("/nuevo")
	private ResponseEntity<Usuario> crearUsuario(){
		
		Usuario usuario = new Usuario();
		usuario.setNombre("PEPE");
		usuario.setPassword("kjashjkashjkdhjkasjdhk");
		
		usuario = repo.save(usuario);
		
		return new ResponseEntity<>(usuario, HttpStatus.OK);
		
	}

	@GetMapping("/obtener/{id}")
	private ResponseEntity<Usuario> obtenerUsuario(@PathVariable("id") Integer id){
				
		
		Optional<Usuario> optional = repo.findById(id);
		
		if(optional.isPresent())
			return new ResponseEntity<>(optional.get(), HttpStatus.OK);
		
		return new ResponseEntity<>(new Usuario(), HttpStatus.NOT_FOUND);
		
	}
	

	@GetMapping("/obtenerPorRol/{idRol}")
	private ResponseEntity<List<Usuario>> obtenerUsuariosPorRol(@PathVariable("idRol") Integer id){
				
		
		List<Usuario> lista = repo.obtenerUsuariosPorRol(id);
		
		return new ResponseEntity<>(lista, HttpStatus.OK);
		
	}
	
}
