package cl.clases.demo2.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import cl.clases.demo2.model.Usuario;

public interface UsuarioRepo extends JpaRepository<Usuario, Integer> {

	
	@Query(" SELECT u FROM Usuario u WHERE u.rol.id=:id ")
	List<Usuario> obtenerUsuariosPorRol(@Param("id") Integer idRol);
	
}
